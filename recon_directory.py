import configparser
import os
import json

class recon_directory:
    
    
    def __init__(self, config_info, list_payload):
        self.config_info = config_info
        self.list_payload = list_payload
        pass

    def readfile_in_url_and_using_ffuf(self):
        #Đọc file url (Các URL cần scan)
        list_url = open(self.config_info["list_url"], "r")

        list_url_readlines = list_url.readlines()

        for i in list_url_readlines:
            i = i.replace("\n","")
            #ffuf -u https://blog.webico.vn/FUZZ -w /home/quockhai/Desktop/Project/recon-tool/SecLists/Discovery/Web-Content/directory-list-2.3-small.txt -ic -o abc.txt -mc 200 -recursion
            cmd = "ffuf -u " + i + "FUZZ" + " -w " + self.list_payload + " -ic" + " -o " + self.config_info["file_output_ffuf"] + " -mc 200 -recursion"
            os.system(cmd)
            self.readfile_report_and_print()


    def readfile_report_and_print(self):
        
        #Đọc file report của ffuf (file report của ffuf sẽ ở dạng json)
        file_report_ffuf = open(self.config_info["file_output_ffuf"], "r")

        file_report_ffuf_readlines = file_report_ffuf.readlines()

        #Chuyển Data về định dạng json
        file_report__ffuf_readlines_json = json.loads(file_report_ffuf_readlines[0])

        #Đọc file report và lưu thông tin về các url có status 200
        file_report_url_200 = open(self.config_info["file_report_url_200"], "a")

        for i in file_report__ffuf_readlines_json['results']:
            file_report_url_200.writelines(i['url'] + '\n')
            


    
